# Heerlen Noord: COMPLECS voorbeeld

Deze HTML pagina wordt door het script in de scripts directory van dit project (en dit Git repository) gegenereerd, en wordt gehost door GitLab Pages. Deze pagina is toegankelijk via https://heerlen-noord.gitlab.io/complecs-voorbeeld (de URL van dit Git repository zelf is https://gitlab.com/heerlen-noord/complecs-voorbeeld).

Om deze pagina opnieuw te genereren, bijvoorbeeld omdat de informatie in de Google Sheet op https://docs.google.com/spreadsheets/d/19elOxiZbbs4ULnFZs96tygvN2i9w6hOuCqdcPPTXE4w is veranderd, kun je naar https://gitlab.com/heerlen-noord/complecs-voorbeeld/-/jobs gaan en op het ronde pijltje achter de bovenste ‘job’ klikken om die ‘job’ te herhalen.
